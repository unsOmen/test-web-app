package ru.omen.app.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping
    public String index(Model model) {
        saveAllUsersInModel(model);
        model.addAttribute("defName", "Default web app");
        return "index";
    }

    @PostMapping
    public String addUser(@RequestParam String userName, @RequestParam String userEmail, Model model) {
        User user = new User(userName, userEmail);
        userRepository.save(user);
        model.addAttribute("defName", user.getName());

        saveAllUsersInModel(model);

        return "index";
    }

    private void saveAllUsersInModel(Model model) {
        Iterable<User> users = userRepository.findAll();
        model.addAttribute("userList", users);
    }
}
